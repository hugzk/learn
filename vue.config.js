module.exports = {
    lintOnSave: false,
    devServer: {
        hot: true, // 它是热更新：只更新改变的组件或者模块，不会整体刷新页面
        proxy: { // 配置代理（只在本地开发有效，上线无效）
            '/api': {
                target: 'http://39.98.123.211', // 这是本地用node写的一个服务，用webpack-dev-server起的服务默认端口是8080
                // changeOrigin: true, // 加了这个属性，那后端收到的请求头中的host是目标地址 target
                // secure: false, // 若代理的地址是https协议，需要配置这个属性
            },
        }
    }
}