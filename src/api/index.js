import ajax from '@/utils/ajax';
export const reqCategoryList = function () {
    return ajax({
        url: '/product/getBaseCategoryList',
        method: 'GET'
    })
}
// rquCategoryList();
import mockAjax from '@/utils/mockAjax'

export const reqGetBanner = () => {
    return mockAjax({
        url: '/banner',
        method: 'GET'
    })
}
export const reqGetFloor = () => {
    return mockAjax({
        url: '/floor',
        method: 'GET'
    })
}

export const reqSearchInfo = (searchParams) => {
    return ajax({
        url: '/list',
        method: 'POST',
        data: searchParams
    })
}
export const reqDetailInfo = (skuId) => {
    return ajax({
        url: `/item/${ skuId }`,
        method: "GET"
    })
}


// api/cart/addToCart/{ skuId }/{ skuNum } 添加或者更新购物车信息

export const reqAddOrUpdataCart = (skuId, skuNum) => {
    return ajax({
        url: `/cart/addToCart/${ skuId }/${ skuNum }`,
        method: 'POST'
    })

}


///api/cart/cartList
export const reqCartList = () => {
    return ajax({
        url: `/cart/cartList`,
        method: 'GET'
    })
}

// /api/cart/checkCart/{skuID}/{isChecked} 改变是否选中 单个

export const reqUpdataChecked = (skuId, isChecked) => {
    return ajax({
        url: `/cart/checkCart/${skuId}/${isChecked}`,
        method: 'get'
    })
}


// opost  /api/cart/batchCheckCart/{isChecked}
// 批量选中购物车
// 参数：skuIdList  数组  代表修改的商品id列表     请求体参数
// 	  isChecked  要修改的状态   1代表选中  0代表未选中

export const reqUpdataCheckedAll = (skuIdList, isChecked) => {
    return ajax({
        url: `/cart/batchCheckCart/${isChecked}`,
        method: 'POST',
        data: skuIdList
    })
}

// /api/cart/deleteCart/{skuId}
export const reqDeleteCart = (skuId) => {
    return ajax({
        url: `/cart/deleteCart/${skuId}`,
        method: 'delete'
    })
}

// DELETE /api/cart/batchDeleteCart

export const reqDeleteCartAllChecked = (skuIdList) => {
    return ajax({
        url: `/cart/batchDeleteCart`,
        method: 'delete',
        data: skuIdList
    })
}

// /api/user/passport/sendCode/{phone}
// 获取验证码
// get
export const reqGetCode = (phone) => {
    return ajax({
        url: `/user/passport/sendCode/${phone}`,
        method: 'get'
    })
}

// /api/user/passport/register
// 注册
// post phone password code
export const reqRegister = (phone, password, code) => {
    return ajax({
        url: `/user/passport/register`,
        method: 'POST',
        data: {
            phone,
            password,
            code
        }
    })
}

// 登录  /api/user/passport/login
// post 
export const reqLogin = (phone, password) => {
    return ajax({
        url: '/user/passport/login',
        method: 'POST',
        data: {
            phone,
            password
        }
    })

}
// 获取用户地址信息
// ///api/user/passport/auth/getUserInfo
// get 
export const reqUserMessage = () => {
    return ajax({
        url: '/user/passport/auth/getUserInfo',
        method: 'GET'
    })
}

// /api/user/passport/logout
// 退出登录
// get 
export const reqLogOut = () => {
    return ajax({
        url: '/user/passport/logout',
        method: 'GET'
    })
}


// /api/user/userAddress/auth/findUserAddressList
// 获取用户地址信息
// get
export const reqUserAddressList = () => {
    return ajax({
        url: `/user/userAddress/auth/findUserAddressList`,
        method: 'GET'
    })
}
// /api/order/auth/trade
// 获取交易页信息
// get
export const reqTradeInfo = () => {
    return ajax({
        url: '/order/auth/trade',
        method: 'GET'
    })
}
// /api/order/auth/submitOrder?tradeNo={tradeNo}
// 生成订单 请求
// post
export const reqOrderInfo = (tradeNo, orderInfo) => {
    return ajax({
        url: `/order/auth/submitOrder?tradeNo=${tradeNo}`,
        method: 'POST',
        data: orderInfo
    })

}

// //api/payment/weixin/createNative/{orderId}  
// 获取订单支付页面信息
// get
export const reqPayInfo = (orderId) => {
    return ajax({
        url: `/payment/weixin/createNative/${orderId}`,
        method: 'GET'
    })
}

// /api/payment/weixin/queryPayStatus/{orderId}
// 获取支付  订单得状态
// get
export const reqPayStatus = (orderId) => {
    return ajax({
        url: `/payment/weixin/queryPayStatus/${orderId}`,
        method: 'GET'
    })
}