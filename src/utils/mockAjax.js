import axios from 'axios';
import nprogress from 'nprogress';
import 'nprogress/nprogress.css'

const service = axios.create({
    baseURL: "/mock",
    timeout: 20000
});
service.interceptors.request.use(config => {
    let usertempid = sessionStorage.getItem('USERTEMPID_KEY');
    config.headers.userTempId = usertempid
    // Do something before request is sent
    nprogress.start();
    return config;
});
service.interceptors.response.use(response => {
    // Do something before response is sent
    nprogress.done()
    return response.data;
}, error => {
    nprogress.done()
    // Do something with response error
    alert('请求失败' + error.message || +'未知错误');
    //中断promise链
    return new Promise(() => {});
    // return Promise.reject(error);
});
export default service;