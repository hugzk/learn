// 引入uuid 生成唯一字符
import {
    v4 as uuidv4
} from 'uuid';

// 判断是否已经具有唯一标识 
export function getUSerTempId() {
    // 获取标识
    let userTempId = localStorage.getItem('USERTEMPID_KEY')
    if (!userTempId) {
        // 生成标识
        userTempId = uuidv4();
        // 浏览器永久存储数据
        localStorage.setItem('USERTEMPID_KEY', userTempId)
    }
    return userTempId
}