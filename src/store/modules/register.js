import { reqGetCode,reqRegister } from "@/api"

const state = {
    code:''
}

const mutations = {
    RECIVE_GETCODE(state,code){
        state.code = code
    }
}
const actions = {
   async getCode({commit},phone){
       let result =  await reqGetCode(phone);
       if(result.code===200){
           commit('RECIVE_GETCODE',result.data);
           return result.data
       }else{
        return Promise.reject(new Error('failed'))
       }
    },
    // 注册
   async register({commit},{phone,password,code}){
       let result =  await reqRegister(phone,password,code);
       if(result.code===200){
           commit('RECIVE_GETCODE',result.data);
           return "ok"
       }else{
        return Promise.reject(new Error('failed'))
       }
    },
}
const getters = {}
export default {
    state,
    actions,
    mutations,
    getters
}