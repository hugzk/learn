import {
    reqTradeInfo,
    reqUserAddressList,
} from "@/api"

const state = {
    tradeInfo: {},
    addressList: []
}
const mutations = {
    RECEIVE_TRADEINFO(state, tradeInfo) {
        state.tradeInfo = tradeInfo
    },
    RECEIVE_ADDRESSLIST(state, addressList) {
        state.addressList = addressList
    }
}
const actions = {
    async getTradeInfo({
        commit
    }) {
        let result = await reqTradeInfo();
        if (result.code === 200) {
            commit('RECEIVE_TRADEINFO', result.data);
        }
    },
    async getUserAddressList({
        commit
    }) {
        let result = await reqUserAddressList();
        if (result.code === 200) {
            commit('RECEIVE_ADDRESSLIST', result.data);
        }
    },
}
const getters = {}
export default {
    state,
    mutations,
    actions,
    getters
}