import {
    reqAddOrUpdataCart
} from '@/api'

const state = {}
const actions = {
    async toShopCart({
        commit
    }, {
        skuId,
        skuNum
    }) {
        let result = await reqAddOrUpdataCart(skuId, skuNum);
        if (result.code === 200) {
            return 'OK'
        } else {
            return Promise.reject(new Error('failed'))
        }
    }
}
const mutations = {}
const getters = {}
export default {
    state,
    actions,
    mutations,
    getters
}