import {
    reqSearchInfo
} from '@/api/index'
let state = {

    searchInfo: {}
}
let mutations = {
    RECEIVE_SEARCHINFO(state, searchInfo) {
        state.searchInfo = searchInfo
    }
}
let actions = {
    async getSearchInfo({
        commit
    }, searchParams) {
        let result = await reqSearchInfo(searchParams);
        if (result.code === 200) {
            commit('RECEIVE_SEARCHINFO', result.data)
        }
    }
}
let getters = {

    attrsList(state) {
        return state.searchInfo.attrsList || []
    },
    goodsList(state) {
        return state.searchInfo.goodsList || []
    },

    trademarkList(state) {
        return state.searchInfo.trademarkList || []
    },
    total(state) {
        return state.searchInfo.total || ''
    }

}
export default {
    state,
    mutations,
    actions,
    getters
}