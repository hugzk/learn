import {
    reqCartList,
    reqUpdataChecked,
    reqUpdataCheckedAll,
    reqAddOrUpdataCart,
    reqDeleteCart,
    reqDeleteCartAllChecked
} from '@/api';
const state = {
    cartList: []
}
const actions = {
    async getCartList({
        commit
    }) {
        let result = await reqCartList();
        if (result.code === 200) {
            commit('RECIVE_GETCARLIST', result.data);
        }
    },

    // 更新单个选中状态
    async getUpdataChecked({
        commit
    }, {
        skuId,
        isChecked
    }) {
        let result = await reqUpdataChecked(skuId, isChecked);
        if (result.code === 200) {
            return 'ok'
        } else {
            return Promise.reject(new Error('failed'))
        }
    },
    // 更改全部选中状态
    async getUpdataCheckedAll({
        commit
    }, {
        skuIdList,
        isChecked
    }) {
        let result = await reqUpdataCheckedAll(skuIdList, isChecked);
        if (result.code === 200) {
            return 'ok'
        } else {
            return Promise.reject(new Error('failed'))
        }
    },
    async addOrUpdataShopCart({
        commit
    }, {
        skuId,
        skuNum
    }) {
        let result = await reqAddOrUpdataCart(skuId, skuNum);
        if (result.code === 200) {
            return 'OK'
        } else {
            return Promise.reject(new Error('failed'))
        }
    },
    async deleteCart({
        commit
    }, skuId) {
        let result = await reqDeleteCart(skuId);
        if (result.code === 200) {
            return 'sucess'
        } else {
            return Promise.reject(new Error('failed'))
        }
    },
    async deleteCartAll({
        commit
    }, skuIdList) {
        let result = await reqDeleteCartAllChecked(skuIdList);
        if (result.code === 200) {
            return 'sucess'
        } else {
            return Promise.reject(new Error('failed'))
        }
    },
}

const mutations = {
    RECIVE_GETCARLIST(state, cartList) {
        state.cartList = cartList
    }
}
const getters = {}
export default {
    state,
    actions,
    mutations,
    getters
}