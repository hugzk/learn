import {
    reqCategoryList,
    reqGetFloor,
    reqGetBanner
} from '@/api/index'
const state = {
    categoryList: [],
    bannerList: [],
    floorList: []
}


const mutations = {
    GETGORYLIST(state, categoryList) {
        state.categoryList = categoryList
    },
    REQGETBANNER(state, bannerList) {
        state.bannerList = bannerList
    },
    REQGETFLOOR(state, floorList) {
        state.floorList = floorList
    }
}


const actions = {
    async getCategoryList({
        commit
    }) {
        let result = await reqCategoryList();
        if (result.code === 200) {
            commit('GETGORYLIST', result.data)
        }

    },
    async getBannerList({
        commit
    }) {
        let result = await reqGetBanner();
        if (result.code === 200) {
            commit('REQGETBANNER', result.data)
        }

    },
    async getFloorList({
        commit
    }) {
        let result = await reqGetFloor();
        if (result.code === 200) {
            commit('REQGETFLOOR', result.data)
        }

    },


}

const getters = {}
export default {
    state,
    actions,
    mutations,
    getters
}