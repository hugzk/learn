 import {
     reqLogin,
     reqLogOut,
     reqUserMessage
 } from '@/api';
 import {
     getUSerTempId
 } from '@/utils/userAbout'
 const state = {
     //  第一次临时标识
     userTempId: getUSerTempId(),
     //  token: '',
     token: localStorage.getItem('TOKEN_KEY'),
     userMessage: {}
 }

 const mutations = {
     RECIVE_LOGIN(state, token) {
         state.token = token
     },
     RECIVE_USERMESSAGE(state, userMessage) {
         state.userMessage = userMessage;
     },
     RESET_TOKEN(state) {
         state.token = '',
             localStorage.removeItem('TOKEN_KEY');
         state.userMessage = {}
     }
 }
 const actions = {
     //  登录获取token
     async login({
         commit
     }, {
         phone,
         password
     }) {
         let result = await reqLogin(phone, password);
         if (result.code === 200) {
             commit('RECIVE_LOGIN', result.data.token)
             // 保存本地永久储存 自动登录
             localStorage.setItem('TOKEN_KEY', result.data.token)

         } else {
            return Promise.reject(new Error('failed'))
         }
     },
     //  获取登录信息
     async getUserMessage({
         commit
     }) {
         let result = await reqUserMessage();
         if (result.code === 200) {
             commit('RECIVE_USERMESSAGE', result.data)
         } else {
            return Promise.reject(new Error('failed'))
         }
     },
     //  重置token 
     resetToken({
         commit
     }) {
         commit('RESET_TOKEN')
     },
     // 退出登录 清空token
     async logOut({
         dispatch
     }) {
         let result = await reqLogOut();
         if (result.code === 200) {
             console.log('wshibaile??')
             dispatch('resetToken')
         } else {

            return Promise.reject(new Error('failed'))
         }
     }
 }
 const getters = {}
 export default {
     state,
     actions,
     mutations,
     getters
 }