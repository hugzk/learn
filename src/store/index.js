import Vue from 'vue'
import Vuex from 'vuex'
import home from '@/store/modules/home';
import user from '@/store/modules/user';
import search from '@/store/modules/search';
import detail from '@/store/modules/detail';
import addOrUpdataCart from '@/store/modules/addOrUpdataCart';
import shopCart from '@/store/modules/shopCart';
import register from "@/store/modules/register";
import trade from '@/store/modules/trade'
Vue.use(Vuex);
const state = {}
const mutations = {}
const actions = {}
const getters = {}

export default new Vuex.Store({
    state,
    mutations,
    actions,
    getters,
    modules: {
        home,
        user,
        search,
        detail,
        addOrUpdataCart,
        shopCart,
        register,
        trade
    }
})