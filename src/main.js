import Vue from 'vue'
import App from '@/App.vue'
import router from '@/router/index'
import store from '@/store/index'
import '@/mock/mockserver'
// 引入swiper组件
import 'swiper/css/swiper.css'
import * as API from '@/api'

import {Button,Message,MessageBox} from 'element-ui';
Vue.use(Button)
Vue.prototype.$msgbox = MessageBox;
Vue.prototype.$alert = MessageBox.alert;
Vue.prototype.$confirm = MessageBox.confirm;
Vue.prototype.$prompt = MessageBox.prompt;
Vue.prototype.$message = Message;


Vue.config.productionTip = false
// 引入全局组件
import BannerSwiper from '@/components/BannerSwiper'
import TypeNav from '@/components/TypeNav'
import Pagintation from '@/components/Pagintation'
Vue.component('TypeNav', TypeNav)
Vue.component('BannerSwiper', BannerSwiper)
Vue.component('Pagintation', Pagintation)
new Vue({
  beforeCreate() {
    Vue.prototype.$bus = this;
    Vue.prototype.$API = API
  },
  render: h => h(App),
  router,
  store
}).$mount('#app')