import mock from 'mockjs';
import floor from '@/mock/floor.json'
import banner from '@/mock/banner.json'
mock.mock('/mock/floor', {
    code: 200,
    data: floor
})
mock.mock('/mock/banner', {
    code: 200,
    data: banner
})