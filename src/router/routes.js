 export default [
     {
         path: '/home',
         component: () =>
             import('@/pages/Home')
     },
     {
         path: '/trade',
         component: () =>
             import('@/pages/Trade')
     },
     {
         path: '/pay',
         component: () =>
             import('@/pages/Pay')
     },
     {
         path: '/paySuccess',
         component: () =>
             import('@/pages/PaySuccess')
     },
     {
         path: '/login',
         component: () =>
             import('@/pages/Login'),
         meta: {
             isHidden: true
         }
     },
     {
         path: '/register',
         component: () =>
             import('@/pages/Register'),
         meta: {
             isHidden: true
         }
     },
     {
         path: '/search/:keyword?',
         component: () =>
             import('@/pages/Search'),
         name: 'search'
     },
     {
         path: '/detail/:skuId',
         component: () =>
             import('@/pages/Detail')
     },
     {
         path: '/addCart/:skuNum',
         component: () =>
             import('@/pages/AddCartSuccess')
     },
     {
         path: '/shopCart',
         component: () => import('@/pages/ShopCart')
     },
     {
         path: '/',
         redirect: '/home'
     }
 ]