import Vue from 'vue';
import Router from 'vue-router';
import routes from '@/router/routes';
// 引入store组件
import store from '@/store/index';
Vue.use(Router);
const configPush = Router.prototype.push;
const configReplace = Router.prototype.replace;
Router.prototype.push = function (loaction, resloved, rejected) {
    if (rejected === undefined && resloved === undefined) {
        return configPush.call(this, loaction).catch(() => {})
    } else {
        return configPush.call(this, loaction, resloved, rejected)
    }
}
Router.prototype.replace = function (loaction, resloved, rejected) {
    if (rejected === undefined && resloved === undefined) {
        return configReplace.call(this, loaction).catch(() => {})
    } else {
        return configReplace.call(this, loaction, resloved, rejected)
    }
}

let router = new Router({
    routes,
    scrollBehavior(to, from, savedPosition) {
        return {
            x: 0,
            y: 0
        }
    },


})
// 添加路由守卫
router.beforeEach(async (to, from, next) => {
    let token = store.state.user.token;
    let userMessage = store.state.user.userMessage;
    if (token) {
        if (to.path === '/login') {
            // 如果还要登录从新跳转到home页面
            next('/')
        } else {
            if (userMessage.name) {
                // 无条件放行
                next()
            } else {
                try {
                    // 调用actions获取用户信息
                    await store.dispatch('getUserMessage');
                    // 成功放行
                    next()
                } catch (error) {
                    // 获取失败  token 重新登录
                    // 清空token清空本地储存得token_key
                    store.dispatch('resetToken');
                    next('/login')

                }

            }
        }
    } else {
        //代表没有登录
        next()


    }
})

export default router;